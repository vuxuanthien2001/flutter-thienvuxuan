import 'package:flutter/material.dart';
import 'package:flutter_vuxuanthien/Basic2_Flutter/HomePage.dart';
import 'package:flutter_vuxuanthien/Basic2_Flutter/RegisterPage.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Welcome Back!",
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    ?.copyWith(fontWeight: FontWeight.bold)),
            const SizedBox(height: 40),
            const TextField(
              decoration: InputDecoration(
                hintText: "Email adress",
                filled: true,
                prefixIcon: Icon(Icons.email),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            TextField(
              obscureText: true,
              decoration: InputDecoration(
                hintText: "Password",
                filled: true,
                prefixIcon: const Icon(Icons.lock),
                suffixIcon: IconButton(icon: const Icon(Icons.visibility_off), onPressed: (){},),
                border: const OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Container(
              alignment: Alignment.centerRight,
              //margin: const EdgeInsets.only(left: 200.0, right: 20.0),
              child: TextButton(
                child: const Text(
                  "Forgot Password?",
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {},
              ),
            ),
            const SizedBox(height: 15),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(shape: const StadiumBorder()),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const MyHome()));
                },
                child: const Text('Sign in'),
              ),
            ),
            const SizedBox(height: 15),
            TextButton(
              child: const Text(
                "I haven't an account",
                style: TextStyle(
                  decoration: TextDecoration.underline,
                  color: Colors.black
                ),
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const RegisterPage()));
              },
            )
          ],
        ),
      )),
    );
  }
}
