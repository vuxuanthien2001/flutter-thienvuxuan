import 'dart:developer';

import 'package:flutter/material.dart';

class MyHome extends StatefulWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  State<MyHome> createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  int _selectIndex = 0;
  PageController pageController = PageController();

  List<String> listurlImage = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQOAXDHKd1hwIIr3H7sq2XOCrRzmzbUecMJuQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTD5S7J8BRJyeDM89bdMqaqib_6r2KWUEbRzQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRH_NkiG9cwdTA1Z0FCZycUeT3EgRg1g28l8g&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLcLrRTU-Y3JFpxlNtEyzUzDvrG8HLMomhwA&usqp=CAU"
  ];

  void _onTapped(int index) {
    setState(() {
      _selectIndex = index;
    });
    pageController.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: pageController,
        children: [
          Column(children: [
            Expanded(child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: listurlImage.length,
              itemBuilder: (context,index){
                return Row(
                  children: [Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(listurlImage[index])),
                  )],
                );
              }))
          ],
            
          ),
          Container(
            color: Colors.blue,
          ),
          Container(
            color: Colors.yellow,
          ),
          Container(
            color: Colors.black,
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '.',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: '.',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timeline_outlined),
            label: '.',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: '.',
          ),
        ],
        currentIndex: _selectIndex,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        onTap: _onTapped,
      ),
    );
  }
}
