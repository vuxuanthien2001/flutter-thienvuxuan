import 'package:flutter/material.dart';

void main() {
  runApp(MyTab());
}

class MyTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: DefaultTabController(
            length: 3,
            child: Scaffold(
              appBar: AppBar(
                bottom: TabBar(
                  tabs: [
                    Tab(text: "Tab 1"),
                    Tab(text: "Tab 2"),
                    Tab(text: "Tab 3")
                  ],
                ),
              ),
              body: TabBarView(children: [
                Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRF0FBtLsVGmmH9VdtDl60egpjVn5VW7M83w&usqp=CAU"),
                Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3-wQxyrUOxdAVMYc2Zrvn4k73XxL3No8ImQ&usqp=CAU"),
                Image.network(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Tj6dAZuCUmVHFd3f07BhOnq-Xwn43uo9XA&usqp=CAU")
              ]),
            )));
  }
}
