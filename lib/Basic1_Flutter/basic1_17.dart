import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyLoadMoreListView());
}

class MyLoadMoreListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Load more ListView",
        theme: ThemeData(
            primaryColor: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity),
        home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List list=[];
  ScrollController _scrollController = ScrollController();
  int _currenMax = 20;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    list = List.generate(_currenMax, (index) => "Item: ${index +1}");
    _scrollController.addListener(() {
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
          _getMoreList();
      }
    });
  }

  _getMoreList(){
    print("Get more list");
    for(int i = _currenMax; i < _currenMax+20; i++){
      list.add("Item: ${i+1}");
    }
    _currenMax = _currenMax +20;
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text ("Load more")),
      body: ListView.builder(
        controller: _scrollController,
        itemExtent: 30,
        itemCount: list.length +1,
        itemBuilder: (context, index){
          if (index == list.length){
            return CupertinoActivityIndicator();
          }
          return ListTile(
            title: Text(list[index]),
          );
        },
        
        ),
      );
  }
}
