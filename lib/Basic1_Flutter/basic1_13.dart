import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyFont());
}

class MyFont extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Font Flutter')
        ),
        body:  Center(
          child: Text(
            'Vũ Xuân Thiện',
            style: GoogleFonts.satisfy()
          ),
        ),
      ),
    );
  }
}
