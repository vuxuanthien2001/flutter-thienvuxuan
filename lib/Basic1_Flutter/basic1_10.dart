import 'package:flutter/material.dart';

void main() {
  runApp(MyCard_SizedBox());
}

class MyCard_SizedBox extends StatelessWidget {
  const MyCard_SizedBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Card - SizedBox'),
        ),
        body: Center(
          child: Card(
            child: SizedBox(
              width: 300,
              height: 100,
              child: Center(child: Text("ahihi"),),
              
            ),
          )
        ),
      ),
    );
  }
}

