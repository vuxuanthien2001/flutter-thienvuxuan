import 'package:flutter/material.dart';

void main() {
  runApp(MyStackLayout());
}

class MyStackLayout extends StatelessWidget {
  const MyStackLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('StackLayout'),
        ),
        body: Center(
          child: Stack(alignment: AlignmentDirectional.center,
          children: [
            Container(width: 500, height: 500, decoration: new BoxDecoration(shape: BoxShape.circle, color: Colors.black),),
            Container(width: 400, height: 400, decoration: new BoxDecoration(shape: BoxShape.circle, color: Colors.yellow),),
            Container(width: 300, height: 300, decoration: new BoxDecoration(shape: BoxShape.circle, color: Colors.red),),
            Container(width: 200, height: 200, decoration: new BoxDecoration(shape: BoxShape.circle, color: Colors.blue),),
            Container(width: 100, height: 100, decoration: new BoxDecoration(shape: BoxShape.circle, color: Colors.white),),
          ],),
        ),
      ),
    );
  }
}

