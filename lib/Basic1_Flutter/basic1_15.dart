import 'package:flutter/material.dart';

void main() {
  runApp(MyLogin());
}

class MyLogin extends StatelessWidget {
  TextEditingController _tk = TextEditingController();
  TextEditingController _mk = TextEditingController();

  MyLogin({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: Center(
          child: Column(
            children: [
              TextFormField(
                controller: _tk,
                decoration: InputDecoration(
                    hintText: "Tài khoản", 
                    labelText: "Username"),
              ),
              TextFormField(
                controller: _mk,
                decoration: InputDecoration(
                    hintText: "Mật khẩu", 
                    labelText: "Password"),
              ),
              ElevatedButton(
                child: Text("Đăng nhập"),
                onPressed: () {
                  if (_tk.text.endsWith("thien") && _mk.text.endsWith("123")) {
                    print("Thành công");
                  } else {
                    print("Thất bại");
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
